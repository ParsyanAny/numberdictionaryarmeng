﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberToWordApp
{
    class ArmNumberDictionary
    {
        static public Dictionary<int, string> NumToWord()
        {
            var dic = new Dictionary<int, string>()
            {    
                #region
                { 0, "Zro" },
                { 1, "Mek" },
                { 2, "Erku" },
                { 3, "Ereq"},
                { 4, "Chors"},
                { 5 ,"Hing"},
                { 6 ,"Wec"},
                { 7 ,"Yot"},
                { 8 ,"Ut"},
                { 9 ,"Iny"},
                { 10 ,"Tas"},
                { 11 ,"Tasnmek" },
                { 12 ,"Tasnerku" },
                { 13 ,"Tasnereq" },
                { 14 ,"Tastnchors" },
                { 15 ,"Tasnhing" },
                { 16 ,"Tasnvec" },
                { 17 ,"Tasnyot" },
                { 18 ,"Tasnut" },
                { 19 ,"Tasniny" },
                { 20 ,"Qsan" },
                { 30 ,"Eresun" },
                { 40 ,"Qarasun" },
                { 50 ,"Hisun" },
                { 60 ,"Watsun" },
                { 70 ,"Yotanasun" },
                { 80 ,"Utsun" },
                { 90 ,"Innysun" },
                { 100 ,"Haryur" },
                { 1000 ,"Hazar" },
                { 1000000 ,"Million" }
#endregion
            };
            return dic;
        }
    }
}
